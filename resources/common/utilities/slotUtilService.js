angular.module("slotUtilService", ["globalUrlService"])
    .factory("slotUtilService",["globalUrlService",function(globalUrlService) {

        var slotUtilServiceAPI = {};
        
        slotUtilServiceAPI.getSlotsMap = function(slots){
            console.log("@@@ creating slotMaps ...");
            var slotMap = [];
            for(var i = 0; i < 7; i++){
                slotMap.push({
                    "slots":[]
                });
            }
            
            getDayInt(slots);
            
            for (var i = 0; i < slots.length ; i++){
                var slot = slots[i];
                if(slotMap[slot.dayInt].slots){
                    slotMap[slot.dayInt].slots.push(slot);
                } else {
                    slotMap[slot.dayInt].slots = [];
                }
                
            }
            return slotMap;
        }      
        
        slotUtilServiceAPI.formatSlotTimes = function(slots){
            for(var i = 0; i < slots.length ; i++){
                var slot = slots[i];
                slot.startTime = slot.startTime.split(":")[0] + ":" + slot.startTime.split(":")[1];
                slot.endTime = slot.endTime.split(":")[0] + ":" + slot.endTime.split(":")[1];
            }
            return slots;
        }
        
        function getDayInt(slots){
             for (var i = 0; i < slots.length ; i++){
                var slot = slots[i];
                if(slot.day === "MONDAY"){
                    slot.dayInt = 0;
                } else if(slot.day === "TUESDAY"){
                    slot.dayInt =1;
                } else if(slot.day === "WEDNESDAY"){
                    slot.dayInt = 2;
                } else if(slot.day === "THURSDAY"){
                    slot.dayInt = 3;
                } else if(slot.day === "FRIDAY"){
                    slot.dayInt = 4;
                } else if(slot.day === "SATURDAY"){
                    slot.dayInt = 5;
                } else if(slot.day === "SUNDAY"){
                    slot.dayInt = 6;
                }
                
            }
        }
        
        return slotUtilServiceAPI;  
        
    }])