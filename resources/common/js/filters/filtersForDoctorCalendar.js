var app = angular.module("filtersForDoctorCalendar",[]);
app.filter('sitefilter', function () {
    return function (arr, genres) {
        var items = {
            genres: genres,
            out: []
        };
        angular.forEach(arr, function (value, key) {
            if (this.genres[value.siteName] === true) {
                this.out.push(value);
            } else if(value.status === "LEAVE"){
                this.out.push(value);
            }
        }, items);
        return items.out;
    };
});
app.filter('doctorfilter', function () {
    return function (arr, genres, siteLength) {
        var items = {
            genres: genres,
            out: []
        };
        angular.forEach(arr, function (value, key) {
            var i = false;
            angular.forEach(value.doctors, function (value1, key) {
                if (items.genres[value1.doctorName] === true) {
                    i = true;
                }  
            })
            if (i) {
                this.out.push(value);
            } else if (value.status === "FREE" && siteLength === 1) {
                this.out.push(value);
            } /*else if (value.status === "CLOSED") {
                this.out.push(value);
            }*/
        }, items);
        return items.out;
    };
});
app.filter('statusfilter', function () {
    return function (arr, genres) {
        var items = {
            genres: genres,
            out: []
        };
        angular.forEach(arr, function (value, key) {
            if (this.genres[value.status] === true) {
                this.out.push(value);
            }
        }, items);
        return items.out;
    };
});