angular.module("globalUrlService", []).service("globalUrlService", ["$http", "$window", "$location", "$rootScope","$state",
    function($http, $window, $location, $rootScope, $state) {
        var globalUrlServiceData = {};

        var location = $location;
        $rootScope.rootContext = location.$$absUrl.split("/resources/")[0] + "/";
//        globalUrlServiceData.data = $rootScope.rootContext;        
//        globalUrlServiceData.data = "http://localhost:8081/";// local        
        globalUrlServiceData.data = "http://chirpn.in:8080/RosterApp/";//chirpn.in        
      //  globalUrlServiceData.data = "/RosterApp/";//Aus Server        

        
        globalUrlServiceData.userData = function(data){ 
            return data; 
        };
        
        $("#dataError1").on("hidden.bs.modal", function () {
            $state.go("home");
        })
        
        globalUrlServiceData.isSession = function(status){ 
            
            if (status === 401 || status === -1) {
                console.log("in session function");
                $("#dataError1").modal("show");
            }
        };
        
        globalUrlServiceData.stringToDate = function(str){ 
            var date = str[6]+str[7]+"/"+str[4]+str[5]+"/"+str[0]+str[1]+str[2]+str[3];
//            var date = str[0]+str[1]+str[2]+str[3]+"-"+str[4]+str[5]+"-"+str[6]+str[7];
            return date;
        };
        
        globalUrlServiceData.toMMDDYYYY = function(date){ 
            var dateArr = date.split("/");
            var dt = dateArr[2] +"-"+ dateArr[1] +"-"+ dateArr[0];
            return dt;
        };
        
        globalUrlServiceData.toHHMMSS = function(sec){ 
            var sec_num = parseInt(sec, 10); // don't forget the second parm
            var hours   = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}
            var time    = hours+":"+minutes+":"+seconds;
            return time;
        };
        
        globalUrlServiceData.dateToString = function(date){ 
            var dateArr = date.split("/");
//            var str = dateArr[0] + dateArr[1] + dateArr[2];
            var str = dateArr[2] + dateArr[1] + dateArr[0];
            return str;
        };
        
        globalUrlServiceData.toSeconds = function(time){ 
            var parts = time.split(":");
            return (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]); 
        };
        
        globalUrlServiceData.serverDateFormatForReports = function(stringDate){
            var arr = stringDate.split("-");
            var date  = arr[0];
            var month = arr[1];
            var year = arr[2];
            // "MM/DD/YYYY"
            return year + "-" + month + "-" + date;
        }
        
        globalUrlServiceData.formatDate = function(date){ 
            var d = new Date(date),
                month = "" + (d.getMonth() + 1),
                day = "" + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = "0" + month;
            if (day.length < 2) day = "0" + day;

            return [year, month, day].join("-");
//            return [day, month, year].join('/');
        };
        
        
        globalUrlServiceData.setStyle = function(cssText) {
            var sheet = document.createElement("style");
            var setStyle;
            sheet.type = "text/css";
            /* Optional */ window.customSheet = sheet;
            (document.head || document.getElementsByTagName("head")[0]).appendChild(sheet);
            return (setStyle = function(cssText, node) {
                if(!node || node.parentNode !== sheet){
                    return sheet.appendChild(document.createTextNode(cssText));
                }
                node.nodeValue = cssText;
                return node;
            })(cssText);
        };
        
        globalUrlServiceData.sortByStringsKeyAsc = function(array, key) {
            return array.sort(function(a, b) {
                var x = a[key].toLowerCase(); var y = b[key].toLowerCase();
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        };
        
        globalUrlServiceData.sortByKeyAsc = function(array, key) {
          
            return array.sort(function(a, b) {
                console.log(a[key]);
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        };
    
        globalUrlServiceData.sortByKeyDesc = function(array, key) {
            return array.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                return ((x > y) ? -1 : ((x < y) ? 1 : 0));
            });
        };
        globalUrlServiceData.getTimeOnly= function (date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }
        
        globalUrlServiceData.addDate = function (date){
            var oldDate = new Date(date);
            var myDate = new Date(oldDate.getTime()+(1*24*60*60*1000));
            dt = globalUrlServiceData.formatDate(myDate);
            console.log('new date for end time : ',dt);
            return dt;
        };
        
        globalUrlServiceData.makeid = function() {
              var text = "";
              var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

              for (var i = 0; i < 10; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

              return text;
            }

        return globalUrlServiceData;
    }]);