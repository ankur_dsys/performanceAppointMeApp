angular.module("ngRightClick",[]).directive('ngRightClick', function ($parse) {
     return function (scope, element, attrs) {
         var fn = $parse(attrs.ngRightClick);
         element.bind('contextmenu', function (event) {
             scope.$apply(function () {
                 event.preventDefault();
                 fn(scope, {
                     $event: event
                 });
                 console.log(event.pageX + "," + event.pageY);
                 console.log('eventeventeventevent : ', event);
                 $("#cntnr").css("left", event.pageX);
                 $("#cntnr").css("top", event.pageY);
                 $("#cntnr").fadeIn(200, startFocusOut());
             });
         });
     };
 });