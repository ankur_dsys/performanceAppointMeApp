angular.module("calendarViewCtrl",["ui.calendar"]).controller("calendarViewCtrl",["$scope","$rootScope",function($scope,$rootScope){
 
     var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    $scope.changeTo = 'Hungarian';

    $rootScope.screenTitle = "Manage Appointment";
    
    $scope.events = [
        {title: 'Long Event',start: new Date()}
    ];

    
    
    $scope.dayClick = function(){
        $("#appointmentModal").modal("show");
    }
    
    
    $scope.events = [
     {title: 'All Day Event',start: new Date(y, m, 1)},
     {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
     {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
     {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
     {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
     {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
   ];
    
     /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };
    
     $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: [ 
          {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
        ]
    };
    
     /* alert on eventClick */
    $scope.alertOnEventClick = function( date, jsEvent, view){
        $scope.alertMessage = (date.title + ' was clicked ');
    };
    /* alert on Drop */
     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
       $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    
     /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    

    
    var out = [];
    $scope.initDashboard = function(){
        
     /*   angular.forEach($scope.appointment,function(value,key){
           
               this.push(value);
        },out)
     */
        
    }
    
    

    var calendar = $("#calendar");
    $scope.uiConfig = {
        calendar: {
            //                     contentHeight: 600,
            //                     aspectRatio: 2,
            editable: false,
            fixedWeekCount:false,
            //                     popover : true,
            displayEventTime: false,
            eventLimit: true, // for all non-agenda views
            views: {
                month: {
                    eventLimit: 7 // adjust to 6 only for agendaWeek/agendaDay
                },
                week: {
                    columnFormat: "ddd DD-MM",            //ddd
                },
                day: {
                    //                            titleFormat: "dddd d MMMM YYYY",
                    columnFormat: "dddd",//"dddd d",           
                }
            },
            header: {
                left: 'prev title next',
                center: '',
                //,right: 'month basicWeek basicDay today',// agendaDay agendaWeek 
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender,
            dayClick: $scope.dayClick,
            changeView: $scope.viewRender,
            eventAfterAllRender: $scope.eventAfterAllRender

        }
    };

    
    

   /* event sources array*/
     $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
     $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];




    /*branch*/
    $scope.branches = [{"branchNameCalendar":"Branch 1(branch)"},{"branchNameCalendar":"Branch 2(branch)"},{"branchNameCalendar":"Branch                        3(branch)"},{"branchNameCalendar":"Branch 4(branch)"},{"branchNameCalendar":"Branch 5(branch)"},                                        {"branchNameCalendar":"Branch 6(branch)"},{"branchNameCalendar":"Branch 7(branch)"},{"branchNameCalendar":"Branch                        8(branch)"},{"branchNameCalendar":"Branch 9(branch)"}]

    /*Staff*/
    $scope.staffInCalendar = [{"staffNameCalendar":"Staff 1 (staff)"},
                              {"staffNameCalendar":"Staff 2(staff)"},
                              {"staffNameCalendar":"Staff 3 (staff)"},
                              {"staffNameCalendar":"Staff 4 (staff)"},
                              {"staffNameCalendar":"Staff 5 (staff)"},
                              {"staffNameCalendar":"Staff 6 (staff)"},
                              {"staffNameCalendar":"Staff 7 (staff)"},
                              {"staffNameCalendar":"Staff 8 (staff)"}]

    /*Service*/
    $scope.serviceInCalendar = [{"serviceNameCalendar":"Service 1"},
                                {"serviceNameCalendar":"Service 2"},
                                {"serviceNameCalendar":"Service 3"},
                                {"serviceNameCalendar":"Service 4"}]

    $scope.selectAllBranch = false;

    $scope.checkAllBranches = function (selected) {
        if(selected){
            $scope.branches.map(function(item){
                item.selectBranch =true;
                $scope.callFunction(selected,item);
            })
        }
        else{
            $scope.branches.map(function(item){
                item.selectBranch =false;
                $scope.callFunction(selected,item);
            })
        }

    };

    $scope.selectedBranch = [];
    $scope.callFunction = function(selected,item){
        
        if(selected){
            var index = $scope.selectedBranch.findIndex(function(x){ return x.branchNameCalendar});
            if(index === -1){
                $scope.selectedBranch.push(item);
            console.log($scope.selectedBranch,"All Checked called");       
            }
            
        }
        else{
            $scope.selectedBranch.splice(item,1);
            console.log("All Unchecked called");       
        }

    }

/*Service List In Model*/
    $scope.serviceList = [{"Sname":"service1"},{"Sname":"service2"},{"Sname":"service3"},{"Sname":"service4"}]

   
}])