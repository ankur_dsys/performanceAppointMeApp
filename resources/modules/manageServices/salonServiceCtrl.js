angular.module('salonServiceCtrl',[]).controller('salonServiceCtrl',['$scope','$state',function($scope,$state){
    $scope.services=[{"id":1,"name":"Waxing","cost":"10$","completionTime":"1 hour"}]
    $scope.initUpdateService=function(){
         $scope.service = $scope.services[0];
    }
    $scope.clickNewService = function(){
        $state.go("addsalonService");
    }
    $scope.service = {};
    $scope.gotToServiceUpdate = function(value){
        $scope.service = $scope.services[0];
        $state.go("updateService");
    }
}])