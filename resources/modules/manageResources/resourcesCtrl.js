angular.module("resourcesCtrl",[]).controller("resourcesCtrl",["$scope","$state",function($scope,$state){
    
    $scope.resources=[{"name":"Shampoo Bowls","usedFor":"Hair Wash","branch":"Morar","staff":"Harry","quantity":"20"}];
    
        $scope.initUpdateResource=function(){
        $scope.resource = $scope.resources[0];
    }
    $scope.clickNewServices = function(){
        $state.go("addResources");
    }
    $scope.resource = {};
    $scope.gotToResourcesUpdate = function(value){
        $scope.resource = $scope.resources[0];
        $state.go("updateResources");
    }
    
    $scope.availableResource = ["Shampoo Bowls","chairs","nails"]
}])