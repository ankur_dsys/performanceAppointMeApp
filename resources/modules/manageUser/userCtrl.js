angular.module("userCtrl",[]).controller("userCtrl",["$scope","dateService","$state",function($scope,dateService,$state){
    
    
    $scope.screenTitle="Manage User";
    $scope.genderList=["Male","Female"];
    
    $scope.roleList=["Admin","Customer","Staff"];
    $scope.userTypeList=["Customer","Staff","Vendor"];
    
    $scope.staffs = [{"fullname":"Harsh Tiwari","email":"harsh@gmail.com","contactNo":"8982119808","address":"Gwalior"}]
    
   var date=dateService.getDate();
   
    $scope.clickAddNewUser=function(){
        
        $state.go('addUser');
    }
    
    $scope.initUpdateUser=function(){
        $scope.staff = $scope.staffs[0];
    }
    
    $scope.clickNewServices = function(){
        $state.go("addUser");
    }
    
    $scope.staff = {};
        $scope.gotToUserUpdate=function(value){
            $scope.staff = $scope.staffs[0];
            $state.go("updateStaff");
        }
    
}])